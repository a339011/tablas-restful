const express = require('express');
const router = express.Router();

const controller = require('../controllers/results.js');
/*GET /results/:n1/:n2 -> Sumar n1 + n2
POST /results/ -> Multiplicar n1 * n2
PUT /results/ -> Dividir n1 / n2
PATCH /results/ -> Potencia n1 ^ n2
DELETE /results/:n1/:n2 -> restar n1 - n2*/


/* GET users listing. */
router.get('/', controller.list);
router.get('/:n1/:n2', controller.sumita);
router.post('/', controller.multiplicadita);
router.put('/', controller.divisionsita);
router.patch('/', controller.potencita);
router.delete('/:n1/:n2', controller.restita);

module.exports = router;